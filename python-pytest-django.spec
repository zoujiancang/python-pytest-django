%global _empty_manifest_terminate_build 0
Name:           python-pytest-django
Version:        4.5.2
Release:        2
Summary:        A Django plugin for pytest.
License:        BSD
URL:            https://pytest-django.readthedocs.io/
Source0:        https://files.pythonhosted.org/packages/9b/42/6d6563165b82289d4a30ea477f85c04386303e51cf4e4e4651d4f9910830/pytest-django-4.5.2.tar.gz
BuildArch:      noarch
%description
Welcome to pytest-django! pytest-django allows you to test your Django
project/applications with the pytest testing tool

%package -n python3-pytest-django
Summary:        A Django plugin for pytest.
Provides:       python-pytest-django
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-setuptools_scm
BuildRequires:  python3-hatchling
# General requires
BuildRequires:  python3-pytest
BuildRequires:  python3-sphinx
BuildRequires:  python3-sphinx_rtd_theme
# General requires
Requires:       python3-pytest
Requires:       python3-sphinx
Requires:       python3-sphinx_rtd_theme
%description -n python3-pytest-django
Welcome to pytest-django! pytest-django allows you to test your Django
project/applications with the pytest testing tool

%package help
Summary:        A Django plugin for pytest.
Provides:       python3-pytest-django-doc
%description help
Welcome to pytest-django! pytest-django allows you to test your Django
project/applications with the pytest testing tool

%prep
%autosetup -n pytest-django-%{version}

%build
%pyproject_build

%install
%pyproject_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

%files -n python3-pytest-django
%{python3_sitelib}/pytest_django
%{python3_sitelib}/pytest_django-*.dist-info/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jul 05 2023 zoujiancang <jczou@isoftstone.com> - 4.5.2-2
- Modify Compiling Mode of package python3-pytest-django to pyproject

* Fri Jul 15 2022 renliang16 <renliang@uniontech.com> - 4.5.2-1
- Upgrade package python3-pytest-django to version 4.5.2

* Tue Jul 20 2021 OpenStack_SIG <openstack@openeuler.org> - 4.1.0-1
- Package Spec generate
